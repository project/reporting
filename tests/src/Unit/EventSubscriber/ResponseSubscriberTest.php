<?php

namespace Drupal\Tests\reporting\Unit\EventSubscriber;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\reporting\Entity\ReportingEndpoint;
use Drupal\reporting\EventSubscriber\ResponseSubscriber;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Tests for the Reporting API Response Subscriber.
 *
 * @coversDefaultClass \Drupal\reporting\EventSubscriber\ResponseSubscriber
 * @group reporting
 */
class ResponseSubscriberTest extends UnitTestCase {

  /**
   * Mock Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  private $entityTypeManager;

  /**
   * Mock Reporting Endpoint Entity Storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  private $endpointStorage;

  /**
   * Mock Reporting Endpoint Entity Query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  private $query;

  /**
   * Mock Reporting Endpoint Entity Type Definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  private $entityTypeDefinition;

  /**
   * Mock URL Generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  private $urlGenerator;

  /**
   * Mock Cache Backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  private $cache;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface|\Prophecy\Prophecy\ObjectProphecy $container */
    $container = $this->prophesize(ContainerInterface::class);
    \Drupal::setContainer($container->reveal());

    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->entityTypeDefinition = $this->prophesize(EntityTypeInterface::class);
    $this->entityTypeDefinition->getLinkTemplates()->willReturn([
      'log' => '/system/reporting/{reporting_endpoint}',
    ]);
    $this->entityTypeDefinition->getKey('langcode')
      ->willReturn(FALSE);
    $this->endpointStorage = $this->prophesize(EntityStorageInterface::class);

    $this->query = $this->prophesize(QueryInterface::class);
    $this->query->condition(Argument::type('string'), Argument::any())
      ->willReturn($this->query->reveal());

    $this->entityTypeManager->getStorage('reporting_endpoint')
      ->willReturn($this->endpointStorage->reveal());
    $this->entityTypeManager->getDefinition('reporting_endpoint')
      ->willReturn($this->entityTypeDefinition->reveal());
    $container->get('entity_type.manager')
      ->willReturn($this->entityTypeManager);

    $this->endpointStorage->getQuery()
      ->willReturn($this->query->reveal());

    $this->urlGenerator = $this->prophesize(UrlGeneratorInterface::class);
    $container->get('url_generator')
      ->willReturn($this->urlGenerator->reveal());

    $this->cache = $this->prophesize(CacheBackendInterface::class);
  }

  /**
   * Empty Reporting-Endpoints should not be added to response.
   */
  public function testNoEndpoints() {
    $this->query->execute()
      ->willReturn([]);

    $this->endpointStorage->loadMultiple([])
      ->shouldNotBeCalled();

    $responseSubscriber = new ResponseSubscriber(
      $this->entityTypeManager->reveal(),
      $this->cache->reveal()
    );

    $response = $this->prophesize(Response::class);
    /** @var \Symfony\Component\HttpFoundation\ResponseHeaderBag|\Prophecy\Prophecy\ObjectProphecy $headers */
    $headers = $this->prophesize(ResponseHeaderBag::class);
    $response->headers = $headers->reveal();

    $event = new ResponseEvent(
      $this->prophesize(HttpKernelInterface::class)->reveal(),
      $this->prophesize(Request::class)->reveal(),
      HttpKernelInterface::MAIN_REQUEST,
      $response->reveal()
    );

    $headers->set('Reporting-Endpoints', Argument::any())
      ->shouldNotBeCalled();

    $responseSubscriber->addReportToHeader($event);
  }

  /**
   * Reporting-Endpoints should be added to response.
   */
  public function testEndpointsHeader() {
    $this->query->execute()
      ->willReturn(['default' => 'default']);

    $this->endpointStorage->loadMultiple(['default' => 'default'])
      ->willReturn([
        'default' => new ReportingEndpoint([
          'id' => 'default',
          'label' => 'Test',
          'status' => 1,
        ], 'reporting_endpoint'),
      ]);

    $this->urlGenerator
      ->generateFromRoute(
        'entity.reporting_endpoint.log',
        Argument::that(function ($value) {
          return $value['reporting_endpoint'] == 'default';
        }),
        Argument::that(function ($value) {
          return $value['absolute'] === TRUE;
        }),
        Argument::type('bool')
      )
      ->willReturn('https://reporting.test/system/reporting/default');

    $responseSubscriber = new ResponseSubscriber(
      $this->entityTypeManager->reveal(),
      $this->cache->reveal()
    );

    $response = $this->prophesize(Response::class);
    /** @var \Symfony\Component\HttpFoundation\ResponseHeaderBag|\Prophecy\Prophecy\ObjectProphecy $headers */
    $headers = $this->prophesize(ResponseHeaderBag::class);
    $response->headers = $headers->reveal();

    $event = new ResponseEvent(
      $this->prophesize(HttpKernelInterface::class)->reveal(),
      $this->prophesize(Request::class)->reveal(),
      HttpKernelInterface::MAIN_REQUEST,
      $response->reveal()
    );

    $headers->set('Reporting-Endpoints', 'default="https://reporting.test/system/reporting/default"')
      ->shouldBeCalled();

    $responseSubscriber->addReportToHeader($event);
  }

  /**
   * Reporting-Endpoints should be added to response with multiple values.
   */
  public function testMultipleEndpoints() {
    $this->query->execute()
      ->willReturn([
        'default' => 'default',
        'csp' => 'csp',
      ]);

    $this->endpointStorage->loadMultiple([
      'default' => 'default',
      'csp' => 'csp',
    ])
      ->willReturn([
        'default' => new ReportingEndpoint([
          'id' => 'default',
          'label' => 'Test',
          'status' => 1,
        ], 'reporting_endpoint'),
        'csp' => new ReportingEndpoint([
          'id' => 'csp',
          'label' => 'Test CSP',
          'status' => 1,
        ], 'reporting_endpoint'),
      ]);

    $this->urlGenerator
      ->generateFromRoute(
        'entity.reporting_endpoint.log',
        Argument::that(function ($value) {
          return $value['reporting_endpoint'] == 'default';
        }),
        Argument::that(function ($value) {
          return $value['absolute'] === TRUE;
        }),
        Argument::type('bool')
      )
      ->willReturn('https://reporting.test/system/reporting/default');
    $this->urlGenerator
      ->generateFromRoute(
        'entity.reporting_endpoint.log',
        Argument::that(function ($value) {
          return $value['reporting_endpoint'] == 'csp';
        }),
        Argument::that(function ($value) {
          return $value['absolute'] === TRUE;
        }),
        Argument::type('bool')
      )
      ->willReturn('https://reporting.test/system/reporting/csp');

    $responseSubscriber = new ResponseSubscriber(
      $this->entityTypeManager->reveal(),
      $this->cache->reveal()
    );

    $response = $this->prophesize(Response::class);
    /** @var \Symfony\Component\HttpFoundation\ResponseHeaderBag|\Prophecy\Prophecy\ObjectProphecy $headers */
    $headers = $this->prophesize(ResponseHeaderBag::class);
    $response->headers = $headers->reveal();

    $event = new ResponseEvent(
      $this->prophesize(HttpKernelInterface::class)->reveal(),
      $this->prophesize(Request::class)->reveal(),
      HttpKernelInterface::MAIN_REQUEST,
      $response->reveal()
    );

    $headers->set(
      'Reporting-Endpoints',
      'default="https://reporting.test/system/reporting/default", csp="https://reporting.test/system/reporting/csp"'
    )->shouldBeCalled();

    $responseSubscriber->addReportToHeader($event);
  }

}
