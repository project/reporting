<?php

namespace Drupal\reporting\EventSubscriber;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Utility\Error;
use Drupal\reporting\ReportingResponse;
use gapple\StructuredFields\Dictionary;
use gapple\StructuredFields\Item;
use gapple\StructuredFields\Serializer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ReportToSubscriber.
 */
class ResponseSubscriber implements EventSubscriberInterface {

  /**
   * The endpoint storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $endpointStorage;

  /**
   * A cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * ResponseSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   A cache bin.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    CacheBackendInterface $cache,
  ) {
    $this->cache = $cache;

    try {
      $this->endpointStorage = $entityTypeManager->getStorage('reporting_endpoint');
    }
    catch (PluginException $e) {
      Error::logException(\Drupal::logger('reporting'), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => [
        ['reportingResponses', 9000],
        ['addReportToHeader', 0],
      ],
    ];
  }

  /**
   * Prevent other response event listeners from altering reporting responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function reportingResponses(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    if ($event->getResponse() instanceof ReportingResponse) {
      $event->stopPropagation();
    }
  }

  /**
   * Add Reporting-Endpoints header to responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function addReportToHeader(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    $cid = 'reporting.response-endpoints';
    $endpoints = [];

    if (($cacheData = $this->cache->get($cid))) {
      $endpoints = $cacheData->data;
    }
    elseif (!empty($this->endpointStorage)) {
      $query = $this->endpointStorage->getQuery()
        ->condition('status', TRUE);

      if (($endpointIds = $query->execute())) {
        $endpointEntities = $this->endpointStorage->loadMultiple($endpointIds);

        foreach ($endpointEntities as $endpointEntity) {
          try {
            // @todo Can local urls be relative?
            $url = $endpointEntity->toUrl('log', ['absolute' => TRUE])->toString();
          }
          catch (EntityMalformedException $e) {
            Error::logException(\Drupal::logger('reporting'), $e);
            continue;
          }
          $endpoints[$endpointEntity->id()] = $url;
        }
      }

      $this->cache->set($cid, $endpoints, Cache::PERMANENT, ['config:reporting_endpoint_list']);
    }

    if (!empty($endpoints)) {
      $endpointsDictionary = new Dictionary();
      array_walk(
        $endpoints,
        function ($url, $id) use ($endpointsDictionary) {
          $endpointsDictionary->{$id} = new Item($url);
        }
      );

      $event->getResponse()->headers->set(
        'Reporting-Endpoints',
        Serializer::serializeDictionary($endpointsDictionary)
      );
    }
  }

}
